﻿using System;
using System.Windows;

namespace FirstSteps.Ioc.WpfClient.Views
{
    public partial class ShellView : Window
    {
        public ShellView()
        {
            InitializeComponent();
        }

        #if DEBUG
        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);
            System.Windows.Application.Current.Shutdown();
        }
        #endif
    }
}
