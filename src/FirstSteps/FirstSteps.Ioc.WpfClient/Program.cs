﻿using System;
using System.Diagnostics;

namespace FirstSteps.Ioc.WpfClient
{
    public static class Program
    {
        [System.STAThreadAttribute()]
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        internal static void Main() => RunApplication((app) => app.Run());

        public static void Run() => RunApplication();

        private static void RunApplication(Action<App> appRun = null)
        {
            try
            {
                App app = new App();
                app.InitializeComponent();
                new Bootstrapper().InitializeShell();
                appRun?.Invoke(app);
            }
            catch (Exception ex)
            {
                //np. NLog --- 
                Debug.WriteLine("--- Fatal Error ---");
                Debug.WriteLine(ex);
            }
        }
    }
}
