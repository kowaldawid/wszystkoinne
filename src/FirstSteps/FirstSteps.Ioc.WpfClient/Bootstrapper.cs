﻿using FirstSteps.Ioc.Application;
using FirstSteps.Ioc.Application.Helpers;
using FirstSteps.Ioc.Service;
using FirstSteps.Ioc.WpfClient.Views;
using Interface;
using MmvvmHelpers.Navigation;
using Models;
using SimpleInjector;
using System;
using System.Linq;
using System.Reflection;

namespace FirstSteps.Ioc.WpfClient
{
    internal class Bootstrapper
    {
        private readonly Container _container;

        public Bootstrapper()
        {
            _container = new Container();

            foreach (var v in AppDomain.CurrentDomain.GetAssemblies()
                .First(r => r.GetName().Name.Contains(nameof(WpfClient)))
                .GetExportedTypes()
                .Where(t => t.Namespace.EndsWith($"{nameof(WpfClient)}.{nameof(Views)}")))
            {
                _container.Register(v);
            }

            _container.Register<IMessage, RequestHelper>(Lifestyle.Singleton);
            //_container.Register<INavigation, Navigation>(Lifestyle.Singleton);

            _container.RegisterInitializer<BaseViewModel>(handlerToInitialize =>
            {
                handlerToInitialize.Message = _container.GetInstance<IMessage>();
                //handlerToInitialize.Navigation = _container.GetInstance<INavigation>();
            });

            foreach (var vm in typeof(BaseViewModel).Assembly.GetExportedTypes()
                .Where(t => !t.IsAbstract && typeof(BaseViewModel).IsAssignableFrom(t)))
            {
                _container.Register(vm);
            }

            Assembly[] businessLayerAssemblies = new[] {
                typeof(IRepository<>).Assembly
            };

            _container.Register<IServiceProxy<IRepository<Example>>, ServiceProxy<IRepository<Example>, Example>>();
            _container.Register(typeof(IRequestHandler<,>), businessLayerAssemblies);
            
            #if DEBUG
            _container.Verify();
            #endif
        }

        public virtual void InitializeShell()
        {
            var shell = _container.GetInstance<ShellView>();
            shell.DataContext = _container.GetInstance<ShellViewModel>();
            shell.Show();
        }
    }
}
