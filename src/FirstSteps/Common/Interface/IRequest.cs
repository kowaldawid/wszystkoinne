﻿using System;

namespace Interface
{
    /// <summary>
    /// Ogólny interfejs rezultatów.
    /// </summary>
    public interface IRequestResult
    {
        Exception Exception { get; set; }
    }

    /// <summary>
    /// Komenda, która zwraca jakiś rezultat.
    /// </summary>
    public interface IRequest<TResult> where TResult : IRequestResult
    {

    }

    /// <summary>
    /// Ogólny interfejs (pomocnik dla request).
    /// </summary>
    public interface IRequestHandler<in TRequest, out TResult>
        where TRequest : IRequest<TResult>
        where TResult : IRequestResult
    {
        TResult Handle(TRequest request);
    }
}
