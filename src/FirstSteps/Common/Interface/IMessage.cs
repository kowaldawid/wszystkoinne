﻿using System;

namespace Interface
{
    public interface IMessage : IDisposable
    {
        //MOŻE KIEDYŚ
        //TResponse Request<TRequest, TResponse>(TRequest message)
        //    where TRequest : class
        //    where TResponse : class;

        TResponse Request<TRequest, TResponse>(TRequest message)
            where TRequest : class, IRequest<TResponse>
            where TResponse : IRequestResult, new();
    }
}
