﻿using MmvvmHelpers;

namespace Models
{
    public class Test : NotifyDataErrorInfo
    {
        private string _test;

        public string Teste
        {
            get => _test;
            set
            {
                SetPropertyChanged(ref _test, value);
            }
        }
    }
}
