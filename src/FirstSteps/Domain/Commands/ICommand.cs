﻿using Interface;

namespace Commands
{
    //MOŻE KIEDYŚ
    ///// <summary>
    ///// Ogólny interfejs rezultatów .
    ///// </summary>
    //public interface ICommandResult
    //{
    //    Exception Exception { get; set; }
    //}

    /// <summary>
    /// Ogólny interfejs rezultatów .
    /// </summary>
    public interface ICommandResult : IRequestResult
    {
    }

    //MOŻE KIEDYŚ
    ///// <summary>
    ///// Komenda, która zwraca jakiś rezultat.
    ///// </summary>
    //public interface ICommand<TResult> where TResult : ICommandResult
    //{
    //}

    /// <summary>
    /// Komenda, która zwraca jakiś rezultat.
    /// </summary>
    public interface ICommand<TResult> : IRequest<TResult>
        where TResult : ICommandResult
    {
    }
}
