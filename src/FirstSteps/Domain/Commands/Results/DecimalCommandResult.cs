﻿using System;

namespace Commands.Results
{
    public class DecimalCommandResult : ICommandResult
    {
        public decimal? Id { get; set; }

        public Exception Exception { get; set; }

        public DecimalCommandResult()
        {
        }

        public DecimalCommandResult(decimal? id = null, Exception exception = null)
        {
            Id = id;
            Exception = exception;
        }
    }
}
