﻿using System;

namespace Commands.Results
{
    public class InformationCommandResult : ICommandResult
    {
        public decimal RejId { get; set; }

        public decimal UserId { get; set; }

        public DateTime DateOfCompletion { get; set; }

        public Exception Exception { get; set; }

        public InformationCommandResult()
        {
        }
    }
}
