﻿using Interface;

namespace Commands
{
    //MOŻE KIEDYŚ
    ///// <summary>
    ///// Ogólny interfejs implementowany przez wszystkie command handlery.
    ///// </summary>
    //public interface ICommandHandler<in TCommand, out TResult>
    //    where TCommand : ICommand<TResult>
    //    where TResult : ICommandResult
    //{
    //    TResult Handle(TCommand command);
    //}

    /// <summary>
    /// Ogólny interfejs implementowany przez wszystkie command handlery.
    /// </summary>
    public interface ICommandHandler<in TCommand, out TResult> : IRequestHandler<TCommand, TResult>
        where TCommand : ICommand<TResult>
        where TResult : ICommandResult
    {

    }
}
