﻿using Commands.Results;

namespace Commands
{
    public class AddExampleCommand : ICommand<InformationCommandResult>
    {
        public string Ex1 { get; set; }

        public string Ex2 { get; set; }
    }
}
