﻿using Interface;

namespace Queries
{
    //MOŻE KIEDYŚ
    ///// <summary>
    ///// Ogólny interfejs rezultatów kwerend.
    ///// </summary>
    //public interface IQueryResult
    //{
    //    Exception Exception { get; set; }
    //}

    /// <summary>
    /// Ogólny interfejs rezultatów kwerend.
    /// </summary>
    public interface IQueryResult : IRequestResult
    {
    }

    //MOŻE KIEDYŚ
    ///// <summary>
    ///// Ogólny interfejs kwerend w systemie.
    ///// </summary>
    //public interface IQuery<TResult> where TResult : IQueryResult 
    //{
    //}

    /// <summary>
    /// Ogólny interfejs kwerend w systemie.
    /// </summary>
    public interface IQuery<TResult> : IRequest<TResult>
        where TResult : IQueryResult
    {
    }
}
