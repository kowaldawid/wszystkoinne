﻿using System;

namespace Queries.Results
{
    public class QueryResult<TData> : IQueryResult
    {
        public Exception Exception { get; set; }

        public TData Data { get; set; }

        public QueryResult()
        {
        }

        public QueryResult(TData data = default(TData), Exception exception = null)
        {
            Exception = exception;
            Data = data;
        }
    }
}
