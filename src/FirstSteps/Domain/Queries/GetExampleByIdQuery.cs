﻿using Models;
using Queries.Results;

namespace Queries
{
    public class GetExampleByIdQuery : IQuery<QueryResult<Example>>
    {
        public int ExampleId { get; set; }
    }
}
