﻿using Interface;

namespace Queries
{
    //MOŻE KIEDYŚ
    ///// <summary>
    ///// Ogólny interfejs implementowany przez wszystkie query handlery.
    ///// </summary>
    //public interface IQueryHandler<in TQuery, out TResult> : IQueryResult
    //    where TQuery : IQuery<TResult>
    //    where TResult : IQueryResult
    //{
    //    TResult Handle(TQuery query);
    //}

    /// <summary>
    /// Ogólny interfejs implementowany przez wszystkie query handlery.
    /// </summary>
    public interface IQueryHandler<in TQuery, out TResult> : IRequestHandler<TQuery, TResult>
        where TQuery : IQuery<TResult>
        where TResult : IQueryResult
    {
    }
}
