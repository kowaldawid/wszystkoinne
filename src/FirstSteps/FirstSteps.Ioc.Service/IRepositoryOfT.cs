﻿using System;
using System.Collections.Generic;

namespace FirstSteps.Ioc.Service
{
    public interface IRepository<TEntity>
        where TEntity : class
    {
        IEnumerable<TEntity> All();
        TEntity FindBy(Func<TEntity, bool> predicate);
        void Add(TEntity entity);
        void Update(TEntity entit);
        void Delete(TEntity entity);
    }
}
