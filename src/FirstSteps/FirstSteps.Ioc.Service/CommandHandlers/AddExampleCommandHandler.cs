﻿using Commands;
using Commands.Results;
using Models;

namespace FirstSteps.Ioc.Service.CommandHandlers
{
    public class AddExampleCommandHandler : ICommandHandler<AddExampleCommand, InformationCommandResult>
    {
        private readonly IServiceProxy<IRepository<Example>> _serviceClient;

        public AddExampleCommandHandler(IServiceProxy<IRepository<Example>> serviceClient)
        {
            _serviceClient = serviceClient;
        }

        public InformationCommandResult Handle(AddExampleCommand request)
        {
            _serviceClient.Execute(x => x.Add(new Example {
                Id = 0,
                Ex1 = "sadas4",
                Ex2 =  "asd4"
            }));

            return new InformationCommandResult();
        }
    }
}
