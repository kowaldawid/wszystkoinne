﻿using Models;
using Queries;
using Queries.Results;
using System;
using System.Collections.Generic;
using System.Linq;

namespace FirstSteps.Ioc.Service.QueryHandlers
{
    public class GetExamplesQueryHandler : IQueryHandler<GetExamplesQuery, QueryResult<IList<Example>>>
    {
        private readonly IServiceProxy<IRepository<Example>> _serviceClient;

        public GetExamplesQueryHandler(IServiceProxy<IRepository<Example>> serviceClient)
        {
            _serviceClient = serviceClient;
        }

        public QueryResult<IList<Example>> Handle(GetExamplesQuery request)
        {
            return new QueryResult<IList<Example>>(_serviceClient.Execute(x => x.All()).ToList());
        }
    }
}
