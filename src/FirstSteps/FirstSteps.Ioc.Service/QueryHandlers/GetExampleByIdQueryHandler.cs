﻿using Models;
using Queries;
using Queries.Results;

namespace FirstSteps.Ioc.Service.QueryHandlers
{
    public class GetExampleByIdQueryHandler : IQueryHandler<GetExampleByIdQuery, QueryResult<Example>>
    {
        private readonly IServiceProxy<IRepository<Example>> _serviceClient;

        public GetExampleByIdQueryHandler(IServiceProxy<IRepository<Example>> serviceClient)
        {
            _serviceClient = serviceClient;
        }

        public QueryResult<Example> Handle(GetExampleByIdQuery request)
        {
            throw new System.NotImplementedException();
        }
    }
}
