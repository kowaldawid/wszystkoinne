﻿using System;

namespace FirstSteps.Ioc.Service
{
    public interface IServiceProxy<TChannel> 
    {
        void Execute(Action<TChannel> operation);

        TResult Execute<TResult>(Func<TChannel, TResult> operation);
    }
}
