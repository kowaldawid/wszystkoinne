﻿using System;
using System.Diagnostics;

namespace FirstSteps.Ioc.Service
{
    public class ServiceProxy<TService, T> : IServiceProxy<TService>
        where TService : class
        where T : class
    {
        public void Execute(Action<TService> operation)
        {
            Debug.WriteLine("--- --- --- ServiceProxy Start: {0} --- --- ---", typeof(TService));
            operation.Invoke((TService)(IRepository<T>)(new FakeRepository<T>()));
        }

        public TResult Execute<TResult>(Func<TService, TResult> operation)
        {
            var result = default(TResult);
            Debug.WriteLine("--- --- --- ServiceProxy Start (Result: {0}): {1} --- --- ---", typeof(TResult), typeof(TService));
            result = operation.Invoke((TService)(IRepository<T>)(new FakeRepository<T>()));
            return result;
        }
    }
}
