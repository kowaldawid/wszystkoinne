﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FirstSteps.Ioc.Service
{
    public class FakeRepository<T> : IRepository<T> where T : class
    {
        public static readonly ICollection<T> fake = new List<T>();

        public T GetChannel() => default(T);

        public void Add(T entity)
        {
            fake.Add(entity);
        }

        public IEnumerable<T> All()
        {
            return fake;
        }

        public void Delete(T entity)
        {
            fake.Remove(entity);
        }

        public T FindBy(Func<T, bool> predicate)
        {
            return fake.SingleOrDefault(predicate);
        }

        public void Update(T entity)
        {
            throw new NotImplementedException();
        }
    }
}
