﻿using Interface;
using SimpleInjector;
using SimpleInjector.Lifestyles;
using System;
using System.Diagnostics;

namespace FirstSteps.Ioc.Application.Helpers
{
    public class RequestHelper : IMessage
    {
        private readonly Container _container;

        public RequestHelper(Container container) => _container = container;

        TResponse IMessage.Request<TRequest, TResponse>(TRequest message)
        {
            TResponse result = default(TResponse);

            if (message == null)
            {
                //np. NLog --- 
                Debug.WriteLine("==== Puste żądanie typu {0} do przetworzenia Warnings ====", typeof(TRequest));
                return result;
            }

            try
            {
                using (var scope = ThreadScopedLifestyle.BeginScope(_container))
                {
                    var handler = scope.GetInstance<IRequestHandler<TRequest, TResponse>>();

                    Debug.WriteLine(string.Format("--- Instancja klasy implementująca {0} została znaleziona {1} ---",
                        typeof(IRequestHandler<TRequest, TResponse>).ToFriendlyName(),
                        handler.GetType()));

                    result = handler.Handle(message);
                }
            }
            catch (Exception ex)
            {
                //Log --- 
                Debug.WriteLine(string.Format("==== Wyjątek podczas przetwarzania żądania {0} Fatal Error ====", typeof(IRequestHandler<TRequest, TResponse>).ToFriendlyName()));
                Debug.WriteLine(ex, "\t--- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---");
                Debug.WriteLine("\t--- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---");

                return new TResponse
                {
                    Exception = ex
                };
            }

            return result;
        }

        public void Dispose() { }
    }
}
