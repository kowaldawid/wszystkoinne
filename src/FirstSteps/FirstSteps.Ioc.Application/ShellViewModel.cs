﻿using Commands;
using Commands.Results;
using MmvvmHelpers.Commands;
using Models;
using Queries;
using Queries.Results;
using System.Collections.Generic;
using System.Windows.Input;

namespace FirstSteps.Ioc.Application
{
    public class ShellViewModel : BaseViewModel
    {
        public ICommand TestCommand { get; private set; }

        public ShellViewModel()
        {
            TestCommand = new RelayCommand(TestAction);
        }

        private void TestAction()
        {
            Message.Request<AddExampleCommand, InformationCommandResult>(new AddExampleCommand());
            var result = Message.Request<GetExamplesQuery, QueryResult<IList<Example>>>(new GetExamplesQuery());
        }
    }
}
