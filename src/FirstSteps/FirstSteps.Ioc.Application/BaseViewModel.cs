﻿using FirstSteps.Ioc.Application.Infrastucture;
using Interface;
using MmvvmHelpers;
using MmvvmHelpers.Navigation;

namespace FirstSteps.Ioc.Application
{
    public abstract class BaseViewModel : NotifyPropertyChanged
    {
        public IMessage Message { get; set; }

        public INavigation Navigation { get; set; }

        protected void NavigationContent(string navigatePath)
        {
            if (navigatePath != null)
                Navigation.RequestNavigate(Regions.ContentRegion, new System.Uri(navigatePath, System.UriKind.Relative));
        }
    }
}
