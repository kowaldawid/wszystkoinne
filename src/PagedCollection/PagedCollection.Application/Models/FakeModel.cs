﻿using MmvvmHelpers;

namespace PagedCollection.Application.Models
{
    public class FakeModel : NotifyPropertyChanged
    {
        public int Id
        {
            get => _id;
            set
            {
                SetPropertyChanged(ref _id, value);
            }
        }

        public string Zrodlo
        {
            get => _zrodlo;
            set
            {
                SetPropertyChanged(ref _zrodlo, value);
            }
        }

        private int _id;
        private string _zrodlo;
    }
}
