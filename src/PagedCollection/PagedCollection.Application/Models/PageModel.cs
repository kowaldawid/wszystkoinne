﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace PagedCollection.Application.Models
{
    public enum Size
    {
        _5 = 5,
        _10 = 10,
        _15 = 15,
        _25 = 25,
        _50 = 50,
        _100 = 100,
    }

    public interface IPage
    {
        //Liczba elmentów
        int PageSize { get; }

        //Bieżąca strona
        int CurrentPage { get; }

        //Łączna liczba stron
        int PageCount { get; }

        void Update(IPage page);
    }

    public class Page : IPage
    {
        public static readonly IEnumerable<int> Size = Enum.GetValues(typeof(Size)).Cast<int>().Select(x => x);

        public int PageSize { get; set; } = (int)Models.Size._25;

        public int CurrentPage { get; set; } = 1;

        public int PageCount { get; set; }

        public Page() { }

        public Page(int currentPage, int pageSize)
        {
            PageSize = pageSize;
            CurrentPage = currentPage;
        }

        public void Update(IPage page)
        {
            PageSize = page.PageSize;
            PageCount = page.PageCount;
            CurrentPage = page.CurrentPage;
        }
    }
}
