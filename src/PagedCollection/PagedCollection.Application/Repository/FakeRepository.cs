﻿using PagedCollection.Application.Models;
using System.Collections.Generic;

namespace PagedCollection.Application.Repository
{
    public static class FakeRepository
    {
        public static IEnumerable<FakeModel> Storage
        {
            get
            {
                for (int i = 0; i < 105; i++)
                    yield return new FakeModel { Id = i, Zrodlo = "Jakies" };
            }
        }
    }
}
