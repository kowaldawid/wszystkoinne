﻿using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;

namespace PagedCollection.Application.Infrastucture
{
    public class ObservableList<T> : IList<T>, INotifyCollectionChanged
        where T : INotifyPropertyChanged
    {
        public event NotifyCollectionChangedEventHandler CollectionChanged;

        public event ListEventHandler AddChanged;

        public event ListEventHandler RemovChanged;

        public event ListEventHandler ClearChanged;

        public delegate void ListEventHandler(IEnumerable<T> sender, T e);

        public int Count => _list.Count;

        public bool IsReadOnly => _list.IsReadOnly;

        public T this[int index]
        {
            get { return _list[index]; }
            set
            {
                _list[index] = value;
                OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Replace, _list[index]));
            }
        }

        public ObservableList() : this(new List<T>()) { }

        public ObservableList(IList<T> list) => _list = list;

        public ObservableList(IEnumerable<T> collection) : this(new List<T>(collection)) { }

        public int IndexOf(T item) => _list.IndexOf(item);

        public void Insert(int index, T item)
        {
            _list.Insert(index, item);
            OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, item, index));
            OnChanged(AddChanged, item);
        }

        public void RemoveAt(int index)
        {
            T temp = _list[index];
            _list.RemoveAt(index);
            OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Remove, temp, index));
            OnChanged(RemovChanged, temp);
        }

        public void Add(T item)
        {
            _list.Add(item);
            OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, item));
            OnChanged(AddChanged, item);
        }

        public void Clear()
        {
            _list.Clear();
            OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
            OnChanged(ClearChanged, default);
        }

        public bool Contains(T item) => _list.Contains(item);

        public void CopyTo(T[] array, int arrayIndex) => _list.CopyTo(array, arrayIndex);

        public bool Remove(T item)
        {
            lock (this)
            {
                var index = IndexOf(item);
                if (_list.Remove(item))
                {
                    OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Remove, item, index));
                    OnChanged(RemovChanged, item);
                    return true;
                }
                return false;
            }
        }

        public void Reset(IList<T> list, bool clearChanged = true)
        {
            _list = list;
            OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));

            if (clearChanged)
                OnChanged(ClearChanged, default);
        }

        public IEnumerator<T> GetEnumerator() => _list.GetEnumerator();

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();

        protected void OnCollectionChanged(NotifyCollectionChangedEventArgs args) => CollectionChanged?.Invoke(this, args);

        protected void OnChanged(ListEventHandler handler, T args) => handler?.Invoke(this, args);

        private IList<T> _list;
    }
}
