﻿using MmvvmHelpers.Commands;
using PagedCollection.Application.Infrastucture;
using PagedCollection.Application.Models;
using PagedCollection.Application.Repository;
using System.Linq;

namespace PagedCollection.Application.ViewModels
{
    public class ExampleDataGridViewModel : ICollectionView<FakeModel>
    {
        public ObservableList<FakeModel> _pageElements { get; private set; }

        public RelayCommand<FakeModel> PodgladCommand { get; private set; }
        public RelayCommand<FakeModel> EdytujCommand { get; private set; }
        public RelayCommand<FakeModel> UsunCommand { get; private set; }
        public RelayCommand DodajCommand { get; private set; }

        public ExampleDataGridViewModel()
        {
            PodgladCommand = new RelayCommand<FakeModel>(PodgladAction);
            EdytujCommand = new RelayCommand<FakeModel>(EdytujAction);
            UsunCommand = new RelayCommand<FakeModel>(UsunAction);
            DodajCommand = new RelayCommand(DodajAction);
        }

        private void DodajAction()
        {
            _pageElements.Add(new FakeModel()
            {
                Id = 500,
                Zrodlo = "Nowe"
            });
        }

        private void UsunAction(FakeModel model)
        {
            _pageElements.Remove(model);
        }

        private void EdytujAction(FakeModel model)
        {
            _pageElements.First(x => x == model).Zrodlo = "Edytowane";
        }

        private void PodgladAction(FakeModel model)
        {
            System.Console.WriteLine(model.Id.ToString());
        }

        public void Load()
        {
            _pageElements = new ObservableList<FakeModel>(FakeRepository.Storage);
        }
    }
}
