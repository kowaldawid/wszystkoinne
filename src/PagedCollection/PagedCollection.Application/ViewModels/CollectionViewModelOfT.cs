﻿using MmvvmHelpers;
using MmvvmHelpers.Infrastucture;
using PagedCollection.Application.Infrastucture;
using PagedCollection.Application.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace PagedCollection.Application.ViewModels
{
    public interface ICollectionView<T> where T : INotifyPropertyChanged
    {
        ObservableList<T> _pageElements { get; }
        void Load();
    }

    public interface ICollectionViewModel<T> : ICollectionViewModel where T : INotifyPropertyChanged
    {
        ICollectionView<T> SelectedTemplate { get; }
    }

    public interface ICollectionViewModel
    {
        int Offset { get; }
        void Sort(string propertyName, ListSortDirection direction);
        void SetFirstPage();
    }

    public class CollectionViewModel<TModel> : NotifyPropertyChanged, ICollectionViewModel<TModel>
        where TModel : INotifyPropertyChanged
    {
        public ICollectionView<TModel> SelectedTemplate { get; private set; }

        public int Offset => _pageParameters != null ? ((_pageParameters.CurrentPage - 1) * _pageParameters.PageSize) + 1 : 1;

        public bool EmptyCollectionVisibility
        {
            get => _emptyCollectionVisibility;
            set
            {
                _emptyCollectionVisibility = value;
                OnPropertyChanged(nameof(CollectionVisibility));
            }
        }

        public bool CollectionVisibility => EmptyCollectionVisibility || SelectedTemplate._pageElements.Count > 0;

        public PagedViewModel PageParameters
        {
            get => _pageParameters;
            set
            {
                _pageParameters = value;

                var paginator = new Paginator(new Page(value.CurrentPage, value.PageSize));

                Refresh();
                PageParameters.Refresh = new MmvvmHelpers.Commands.RelayCommand<IPage>((e) => SelectedTemplate._pageElements.Reset(paginator.Paginate(e).ToList(), false));

                SelectedTemplate._pageElements.AddChanged += (s, e) => Refresh();
                SelectedTemplate._pageElements.RemovChanged += (s, e) => Refresh();
                SelectedTemplate._pageElements.ClearChanged += (s, e) => Refresh();

                void Refresh()
                {
                    SelectedTemplate._pageElements.Reset(paginator.Update(_source).ToList(), false);
                    PageParameters.Update(paginator.Page);
                }
            }
        }

        public CollectionViewModel(ICollectionView<TModel> TViewModel)
        {
            SelectedTemplate = TViewModel;
            SelectedTemplate.Load();
            _source = SelectedTemplate._pageElements.ToList();

            SelectedTemplate._pageElements.AddChanged += (s, e) => Refresh(() => { _source.Add(e); });
            SelectedTemplate._pageElements.RemovChanged += (s, e) => Refresh(() => { _source.Remove(e); });
            SelectedTemplate._pageElements.ClearChanged += (s, e) => Refresh(() => _source = s.ToList());

            void Refresh(Action p)
            {
                p?.Invoke();
                OnPropertyChanged(nameof(CollectionVisibility));
                _source = _sorter.Sort(_source);

                if (_pageParameters == null)
                    SelectedTemplate._pageElements.Reset(_source.ToList(), false);
            }
        }

        public void Sort(string propertyName, ListSortDirection direction)
        {
            _sorter.SetSort(propertyName, direction);
            SelectedTemplate._pageElements.Reset(_source);
        }

        public void SetFirstPage() => _pageParameters?.FirstPageCommand.Execute();

        private PagedViewModel _pageParameters;
        private bool _emptyCollectionVisibility = true;
        private IList<TModel> _source;
        private readonly Sorter _sorter = new Sorter();

        private sealed class Sorter
        {
            public void SetSort(string propertyName, ListSortDirection direction)
            {
                this.propertyName = propertyName;
                this.direction = direction;
            }

            public IList<TModel> Sort(IList<TModel> _source)
            {
                if (propertyName == null) return _source;

                System.Reflection.PropertyInfo info = typeof(TModel).GetProperty(propertyName);
                if (direction == ListSortDirection.Descending)
                    return _source.OrderByDescending(s => info.GetValue(s)).ToList();
                else
                    return _source.OrderBy(s => info.GetValue(s)).ToList();
            }

            private string propertyName;
            private ListSortDirection direction;
        }

        private sealed class Paginator
        {
            public IPage Page { get; private set; }

            public Paginator(IPage page) => Page = page;

            public IEnumerable<TModel> Update(IEnumerable<TModel> updates)
            {
                all = updates;
                return Paginate();
            }

            public IEnumerable<TModel> Paginate(IPage parameters)
            {
                if (parameters == null || parameters.CurrentPage < 0 || parameters.PageSize < 1
                    || (parameters.PageSize == Page.PageSize && parameters.CurrentPage == Page.CurrentPage))
                    return current;

                Page = parameters;
                return Paginate();
            }

            private IEnumerable<TModel> Paginate()
            {
                int pageCount = CalculatePages();
                int currentPage = Page.CurrentPage > pageCount ? pageCount : Page.CurrentPage;
                int skip = Page.PageSize * (currentPage - 1);
                Page.Update(new Page
                {
                    PageCount = pageCount,
                    CurrentPage = currentPage,
                    PageSize = Page.PageSize
                });
                return current = all.Skip(skip).Take(Page.PageSize).ToList();
            }

            private int CalculatePages()
            {
                var count = all.Count();
                if (Page.PageSize >= count)
                    return 1;

                int pages = count / Page.PageSize;
                return (count % Page.PageSize == 0) ? pages : pages + 1;
            }

            private IEnumerable<TModel> all;
            private IEnumerable<TModel> current;
        }
    }
}
