﻿using PagedCollection.Application.Infrastucture;
using PagedCollection.Application.Models;
using PagedCollection.Application.Repository;

namespace PagedCollection.Application.ViewModels
{
    public class ExampleListViewModel : ICollectionView<FakeModel>
    {
        public ObservableList<FakeModel> _pageElements { get; private set; }

        public void Load() => _pageElements = new ObservableList<FakeModel>(FakeRepository.Storage);
    }
}
