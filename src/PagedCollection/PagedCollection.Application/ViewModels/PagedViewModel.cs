﻿using MmvvmHelpers;
using MmvvmHelpers.Commands;
using PagedCollection.Application.Models;

namespace PagedCollection.Application.ViewModels
{
    public class PagedViewModel : NotifyPropertyChanged
    {
        public RelayCommand NextPageCommand { get; private set; }
        public RelayCommand PreviousPageCommand { get; private set; }
        public RelayCommand FirstPageCommand { get; private set; }
        public RelayCommand LastPageCommand { get; private set; }
        public RelayCommand<IPage> Refresh { get; set; }

        public int PageCount
        {
            get => _pageCount;
            private set => SetPropertyChanged(ref _pageCount, value);
        }

        public int CurrentPage
        {
            get => _currentPage;
            private set => SetPropertyChanged(ref _currentPage, value);
        }

        public int PageSize
        {
            get => _pageSize;
            set
            {
                if (_pageSize == value)
                    return;

                _pageSize = value;
                var temp = new Page(_currentPage, _pageSize);
                Refresh?.Execute(temp);
                Update(temp);
            }
        }

        public PagedViewModel(int currentPage, Size pageSize)
        {
            _currentPage = currentPage;
            _pageSize = (int)pageSize;

            NextPageCommand = new RelayCommand(() => SetPage(CurrentPage + 1), () => CurrentPage < PageCount);
            PreviousPageCommand = new RelayCommand(() => SetPage(CurrentPage - 1), () => CurrentPage > 1);
            FirstPageCommand = new RelayCommand(() => SetPage(1), () => CurrentPage > 1);
            LastPageCommand = new RelayCommand(() => SetPage(PageCount), () => CurrentPage < PageCount);
        }

        public void Update(IPage page)
        {
            CurrentPage = page.CurrentPage;
            PageSize = page.PageSize;
            PageCount = page.PageCount;
        }

        private void SetPage(int i)
        {
            CurrentPage = i;
            Refresh?.Execute(new Page(_currentPage, _pageSize));
        }

        private int _currentPage;
        private int _pageCount;
        private int _pageSize;
    }
}
