﻿using MmvvmHelpers.Infrastucture;
using PagedCollection.Application.ViewModels;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace PagedCollection.Presentation.Helpers
{
    public static class ICollectionViewModelHelper
    {
        public static CollectionViewModel<TModel> Register<TModel, TView>(this CollectionViewModel<TModel> test, ResourceDictionary resourceDictionary)
            where TModel : INotifyPropertyChanged
        {
            DataTemplateManager.RegisterDataTemplate(test.SelectedTemplate.GetType(), typeof(TView), resourceDictionary);
            return test;
        }

        public static ICollectionViewModel GetICollectionViewModel(this DataGrid dg)
        {
            var t = dg.Parent;
            while (t != null)
            {
                t = VisualTreeHelper.GetParent(t);
                if ((t as ContentControl)?.DataContext is ICollectionViewModel sortable)
                {
                    return sortable;
                }
            }
            return null;
        }
    }
}
