﻿using PagedCollection.Presentation.Helpers;
using System.Windows;
using System.Windows.Controls;

namespace PagedCollection.Presentation.Behaviour
{
    public static class DisplayRowNumberBehaviour
    {
        #region Behaviors:DisplayRowNumberBehaviour.RowNumber
        public static bool GetRowNumber(DependencyObject dependency)
        {
            return (bool)dependency.GetValue(DisplayRowNumberBehaviourProperty);
        }

        public static void SetRowNumber(DataGrid dependency, bool value)
        {
            dependency.SetValue(DisplayRowNumberBehaviourProperty, value);
        }

        public static readonly DependencyProperty DisplayRowNumberBehaviourProperty =
            DependencyProperty.RegisterAttached("RowNumber", typeof(bool),
            typeof(DisplayRowNumberBehaviour), new FrameworkPropertyMetadata(false, OnDisplayRowNumberChanged));
        #endregion

        private static void OnDisplayRowNumberChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (!(d is DataGrid refDG) || !(bool)e.NewValue) return;

            refDG.LoadingRow += HandleLoadedRow;
        }

        private static void HandleLoadedRow(object sender, DataGridRowEventArgs e)
        {
            var refDO = sender as DataGrid;
            if (!GetRowNumber(refDO))
            {
                refDO.LoadingRow -= HandleLoadedRow;
                return;
            }

            e.Row.Header = refDO.GetICollectionViewModel()?.Offset + e.Row.GetIndex();
        }
    }
}
