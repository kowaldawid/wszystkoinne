﻿using PagedCollection.Presentation.Helpers;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;

namespace PagedCollection.Presentation.Behaviour
{
    public static class SortBehaviour
    {
        #region Behaviors:SortBehaviour.Sort
        public static bool GetSort(DataGrid grid)
        {
            return (bool)grid.GetValue(AllowCustomSortProperty);
        }

        public static void SetSort(DataGrid grid, bool value)
        {
            grid.SetValue(AllowCustomSortProperty, value);
        }

        public static readonly DependencyProperty AllowCustomSortProperty =
            DependencyProperty.RegisterAttached("Sort", typeof(bool),
            typeof(SortBehaviour), new UIPropertyMetadata(false, OnSortChanged));
        #endregion 

        private static void OnSortChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (!(d is DataGrid refDG)) return;

            var oldV = (bool)e.OldValue;
            var newV = (bool)e.NewValue;

            if (!oldV && newV)
            {
                refDG.CanUserSortColumns = true;
                refDG.IsVisibleChanged += ClearSort;
                refDG.Sorting += HandleCustomSorting;
            }
            else
            {
                refDG.CanUserSortColumns = false;
                refDG.IsVisibleChanged -= ClearSort;
                refDG.Sorting -= HandleCustomSorting;
            }

        }

        private static void ClearSort(object sender, DependencyPropertyChangedEventArgs e)
        {
            if ((e.NewValue is bool t) && t || !(sender is DataGrid refDG) || refDG.Items.Count > 0) return;

            foreach (var c in refDG.Columns)
                c.SortDirection = null;

            refDG.GetICollectionViewModel()?.Sort(null, default);
        }

        private static void HandleCustomSorting(object sender, DataGridSortingEventArgs e)
        {
            e.Handled = true;

            DataGridColumn column = e.Column;
            ListSortDirection direction;

            if (!(sender is DataGrid refDG) || !GetSort(refDG)) return;

            column.SortDirection = direction = (column.SortDirection != ListSortDirection.Ascending)
                                ? ListSortDirection.Ascending
                                : ListSortDirection.Descending;

            refDG.GetICollectionViewModel()?.Sort(column.SortMemberPath, direction);
        }
    }
}
