﻿using PagedCollection.Application.Models;
using PagedCollection.Application.ViewModels;
using PagedCollection.Presentation.Helpers;
using PagedCollection.Presentation.Views.Shared;
using Size = PagedCollection.Application.Models.Size;

namespace PagedCollection.Presentation
{
    internal static class ViewModelLocator
    {
        public static ICollectionViewModel<FakeModel> ExampleListViewModel
            => new CollectionViewModel<FakeModel>(new ExampleListViewModel())
            {
                PageParameters = new PagedViewModel(1, Size._15),
                EmptyCollectionVisibility = false
            }.Register<FakeModel, ExampleListViewView>(System.Windows.Application.Current.Resources);

        public static ICollectionViewModel<FakeModel> ExampleDataGridViewModel
            => new CollectionViewModel<FakeModel>(new ExampleDataGridViewModel())
            {
                PageParameters = new PagedViewModel(1, Size._10),
            }.Register<FakeModel, ExampleDataGridView>(System.Windows.Application.Current.Resources);
    }
}
