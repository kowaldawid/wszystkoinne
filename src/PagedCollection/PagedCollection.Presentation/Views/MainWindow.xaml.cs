﻿using System.Windows;

namespace PagedCollection.Presentation.Views
{
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            new ExampletView().ShowDialog();
            new CollectionWindowView().SetCollectionViewModel(ViewModelLocator.ExampleListViewModel).ShowDialog();
        }
    }
}
