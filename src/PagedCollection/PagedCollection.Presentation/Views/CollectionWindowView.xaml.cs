﻿using PagedCollection.Application.ViewModels;
using System.ComponentModel;
using System.Windows;

namespace PagedCollection.Presentation.Views
{
    public partial class CollectionWindowView : Window
    {
        public CollectionWindowView SetCollectionViewModel<T>(ICollectionViewModel<T> value) where T : INotifyPropertyChanged
        {
            DataContext = value;
            return this;
        }

        public CollectionWindowView()
        {
            InitializeComponent();
        }
    }
}
