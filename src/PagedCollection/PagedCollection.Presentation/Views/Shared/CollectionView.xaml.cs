﻿using PagedCollection.Application.ViewModels;
using System.ComponentModel;
using System.Windows.Controls;

namespace PagedCollection.Presentation.Views.Shared
{
    public partial class CollectionView : UserControl
    {
        public CollectionView SetCollectionViewModel<T>(ICollectionViewModel<T> value) where T : INotifyPropertyChanged
        {
            DataContext = value;
            return this;
        }

        public CollectionView()
        {
            InitializeComponent();
        }
    }
}
