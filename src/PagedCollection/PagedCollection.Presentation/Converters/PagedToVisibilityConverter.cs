﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace PagedCollection.Presentation.Converters
{
    public class PagedToVisibilityConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture) =>
            values[0] is int && (int)values[0] > 1 ? Visibility.Visible : Visibility.Collapsed;

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture) => throw new NotSupportedException("Cannot convert back");
    }
}
