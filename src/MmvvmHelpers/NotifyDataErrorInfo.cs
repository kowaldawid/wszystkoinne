﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.CompilerServices;

namespace MmvvmHelpers
{
    /// <summary>
    /// Walidacja <see cref="INotifyDataErrorInfo"/>
    /// <see href="https://github.com/anthyme/AsyncValidation"/>
    /// Uwaga jeśli walidacja spowalnia GUI trzeba walidację wrzucić do wątku patrz <see cref="ConcurrentDictionary{TKey, TValue}"/>
    /// Przy wątku Dictionary podmięnić na ConcurrentDictionary, a _errors.Remove/Add(...) podmięnić na _errors.TryRemove/TryAdd(...)
    /// Kontrola momentu sprwadzania poprawności
    /// Ułatwia unieważnienie właściwości podczas ustawiania innej właściwości
    /// 
    /// <see href="https://www.nuget.org/packages/System.Reactive/"/>
    /// <see href="https://rehansaeed.com/reactive-extensions-part1-replacing-events/"/>
    /// </summary>
    public abstract class NotifyDataErrorInfo : NotifyPropertyChanged, INotifyDataErrorInfo
    {
        /// <summary>
        /// Czy obiekt("właściwość") ma błedy sprawdzenia poprawności.
        /// </summary>
        /// <value><c>true</c> jeśli zawiera błęd, w przeciwnym razie <c>false</c></value>
        public bool HasErrors { get { return _errors.Any(x => x.Value?.Count > 0); } }

        /// <summary>
        /// Event informujący o zmianie stanu błedu.
        /// </summary>
        public event EventHandler<DataErrorsChangedEventArgs> ErrorsChanged;

        /// <summary>
        /// Sprawdzenie poprawności obiektu (dla właściwości ustawionych w atrybucie).
        /// </summary>
        public void Validate()
        {
            //Służy do sprawdzenia poprawności obiektów.
            ValidationContext validationContext = new ValidationContext(this, null, null);
            List<ValidationResult> validationResults = new List<ValidationResult>();
            //Określa, czy obiekt jest nieprawidłowy.
            Validator.TryValidateObject(this, validationContext, validationResults, true);

            _errors.ToList().ForEach(er =>
            {
                if (validationResults.All(r => r.MemberNames.All(m => m != er.Key)))
                {
                    _errors.Remove(er.Key);
                    OnErrorsChanged(er.Key);
                }
            });

            IEnumerable<IGrouping<string, ValidationResult>> members = validationResults.SelectMany(r => r.MemberNames.GroupBy(m => m, m => r));

            foreach (var m in members)
            {
                var messages = m.Select(x => x.ErrorMessage).ToList();
                if (_errors.ContainsKey(m.Key))
                {
                    _errors.Remove(m.Key);
                }
                _errors.Add(m.Key, messages);
                OnErrorsChanged(m.Key);
            }
        }

        /// <param name="propertyName">
        /// Nazwa właściwości. 
        /// Pole może zsotać dostarczone automatycznie patrz <see cref="CallerMemberNameAttribute" />
        /// </param>
        /// <returns>Zwraca listę błędów.</returns>
        public IEnumerable GetErrors([CallerMemberName] string propertyName = null)
        {
            _errors.TryGetValue(propertyName ?? string.Empty, out List<string> errorsForName);
            return errorsForName;
        }

        /// <summary>
        /// Powiadomienie gdy błedy ulegną zmianie. 
        /// </summary>
        /// <param name="propertyName">
        /// Nazwa właściwości. 
        /// Pole może zsotać dostarczone automatycznie patrz <see cref="CallerMemberNameAttribute" />
        /// </param>
        protected virtual void OnErrorsChanged([CallerMemberName] string propertyName = null)
        {
            ErrorsChanged?.Invoke(this, new DataErrorsChangedEventArgs(propertyName));
        }

        /// <summary>
        /// Powiadomienie o zmianie stanu właściwości. 
        /// Wywołanie walidacji.
        /// </summary>
        protected override void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            base.OnPropertyChanged(propertyName);
            Validate();
        }

        /// <summary>
        /// Key, klucz do property (nazwa właściwości).
        /// Values Reprezentacja stanu sprawdzenia poprawności modelu.
        /// </summary>
        private readonly Dictionary<string, List<string>> _errors = new Dictionary<string, List<string>>();
    }
}
