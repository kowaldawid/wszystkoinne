﻿using System;
using System.Windows;
using System.Windows.Markup;

namespace MmvvmHelpers.Infrastucture
{
    public static class DataTemplateManager
    {
        public static void RegisterDataTemplate<TViewModel, TView>(ResourceDictionary resourceDictionary)
            where TView : FrameworkElement
        {
            RegisterDataTemplate(typeof(TViewModel), typeof(TView), resourceDictionary);
        }

        public static void RegisterDataTemplate(Type viewModelType, Type viewType, ResourceDictionary resourceDictionary)
        {
            foreach (var item in resourceDictionary.Keys)
                if (item is DataTemplateKey && ((DataTemplateKey)item).DataType.Equals(viewModelType)) return;

            var template = CreateTemplate(viewModelType, viewType);
            var key = template.DataTemplateKey;
            resourceDictionary.Add(key, template);
        }

        private static DataTemplate CreateTemplate(Type viewModelType, Type viewType)
        {
            const string xamlTemplate = "<DataTemplate DataType=\"{{x:Type viewModels:{0}}}\"><view:{1} /></DataTemplate>";
            string xaml = String.Format(xamlTemplate, viewModelType.Name, viewType.Name);
            ParserContext context = new ParserContext
            {
                XamlTypeMapper = new XamlTypeMapper(new string[0])
            };

            context.XamlTypeMapper.AddMappingProcessingInstruction("viewModels", viewModelType.Namespace, viewModelType.Assembly.FullName);
            context.XamlTypeMapper.AddMappingProcessingInstruction("view", viewType.Namespace, viewType.Assembly.FullName);
            context.XmlnsDictionary.Add("", "http://schemas.microsoft.com/winfx/2006/xaml/presentation");
            context.XmlnsDictionary.Add("x", "http://schemas.microsoft.com/winfx/2006/xaml");
            context.XmlnsDictionary.Add("viewModels", "viewModels");
            context.XmlnsDictionary.Add("view", "view");

            return (DataTemplate)XamlReader.Parse(xaml, context);
        }
    }

}
