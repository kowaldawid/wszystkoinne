﻿using System;

namespace MmvvmHelpers.Commands
{
    /// <summary>
    /// Async -> <see href="https://johnthiriet.com/mvvm-going-async-with-async-command/"/>
    /// Kiedy <see cref="ICommand"/> nie przyjmuje żadnych parametrów dla <see cref="Execute()"/> oraz <see cref="CanExecute()"/>
    /// <see cref="CommandBase"/>
    /// <see cref="RelayCommand{T}"/>
    /// </summary>
    public class RelayCommand : CommandBase
    {

        /// <param name="execute">Wywołanie <see cref="Action"/> gdy <see cref="ICommand.Execute"/> zostanie wywołane</param>
        public RelayCommand(Action execute) : this(execute, () => true) { }

        /// <param name="execute">Wywołanie <see cref="Action"/> gdy <see cref="ICommand.Execute"/> zostanie wywołane</param>
        /// <param name="canExecute">Wywołanie <see cref="Func{TResult}"/> gdy <see cref="ICommand.CanExecute"/> zostanie wywołane</param>
        public RelayCommand(Action execute, Func<bool> canExecute) : base()
        {
            _execute = execute ?? throw new ArgumentNullException(nameof(execute));
            _canExecute = canExecute ?? throw new ArgumentNullException(nameof(canExecute));
        }

        public void Execute() => _execute();

        /// <returns>Zwraca <see langword="true"/> jeśli polecenie może zostać wykonane w przeciwnym razie <see langword="false"/>.</returns>
        public bool CanExecute() => _canExecute();

        /// <summary>
        /// Wywołanie <see cref="ICommand.Execute(object)"/>
        /// </summary>
        /// <param name="parameter">Parametr polecenia</param>
        protected override void Execute(object parameter) => Execute();

        /// <summary>
        /// Wywołanie <see cref="ICommand.CanExecute(object)"/>
        /// </summary>
        /// <param name="parameter">Parametr polecenia</param>
        /// <returns>Zwraca <see langword="true"/> jeśli polecenie można wykonać, w przeciwnym razie <see langword="false" /></returns>
        protected override bool CanExecute(object parameter) => CanExecute();

        private readonly Action _execute;
        private readonly Func<bool> _canExecute;
    }
}
