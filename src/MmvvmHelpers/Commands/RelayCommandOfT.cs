﻿using System;
using System.Diagnostics;
using System.Reflection;

namespace MmvvmHelpers.Commands
{
    /// <summary>
    /// Async -> <see href="https://johnthiriet.com/mvvm-going-async-with-async-command/"/>
    /// Kiedy <see cref="ICommand"/> przyjmuje parametry dla <see cref="Execute()"/> oraz <see cref="CanExecute()"/>
    /// <typeparam name="T">Typ parametru</typeparam>
    /// </summary>
    public class RelayCommand<T> : CommandBase
    {
        /// <param name="execute">Wywołanie <see cref="Action{T}"/> gdy <see cref="ICommand.Execute"/> zostanie wywołane</param>
        public RelayCommand(Action<T> execute) : this(execute, (x) => true) { }

        /// <param name="execute">Wywołanie <see cref="Action{T}"/> gdy <see cref="ICommand.Execute"/> zostanie wywołane</param>
        /// <param name="canExecute">Wywołanie <see cref="Func{T, TResult}"/> gdy <see cref="ICommand.CanExecute"/> zostanie wywołane</param>
        public RelayCommand(Action<T> execute, Func<T, bool> canExecute) : base()
        {
            TypeInfo genericTypeInfo = typeof(T).GetTypeInfo();

            if ((genericTypeInfo.IsValueType && (genericTypeInfo.IsGenericType) && (typeof(Nullable<>).GetTypeInfo().IsAssignableFrom(genericTypeInfo.GetGenericTypeDefinition().GetTypeInfo())))
                || genericTypeInfo.AsType().Equals(typeof(Nullable)))
                throw new InvalidCastException("Delegat " + nameof(execute) + " oraz " + nameof(canExecute) + " nie mogą być typu Nullable<>");

            _execute = execute ?? throw new ArgumentNullException(nameof(execute));
            _canExecute = canExecute ?? throw new ArgumentNullException(nameof(canExecute));
        }

        ///<param name="parameter">Dane używanee przez polecenie</param>
        public void Execute(T parameter) => _execute(parameter);

        ///<param name="parameter">Dane używanee przez polecenie do ustalenia, czy może zostać wykonane</param>
        ///<returns>
        /// <returns>Zwraca <see langword="true"/> jeśli polecenie może zostać wykonane w przeciwnym razie <see langword="false"/>.</returns>
        ///</returns>
        [DebuggerStepThrough]
        public bool CanExecute(T parameter) => _canExecute(parameter);

        /// <summary>
        /// Wywołanie <see cref="ICommand.Execute(object)"/>
        /// </summary>
        /// <param name="parameter">Parametr polecenia</param>
        protected override void Execute(object parameter) => Execute(Convert(parameter));

        /// <summary>
        /// Wywołanie <see cref="ICommand.CanExecute(object)"/>
        /// </summary>
        /// <param name="parameter">Parametr polecenia</param>
        /// <returns>Zwraca <see langword="true"/> jeśli polecenie można wykonać, w przeciwnym razie <see langword="false" /></returns>
        protected override bool CanExecute(object parameter) => CanExecute(Convert(parameter));

        private T Convert(object parameter)
        {
            if (parameter == null) return default(T);
            else if (parameter is IComparable) return (T)System.Convert.ChangeType(parameter, typeof(T));
            else return (T)parameter;
        }

        private readonly Action<T> _execute;
        private readonly Func<T, bool> _canExecute;
    }
}
