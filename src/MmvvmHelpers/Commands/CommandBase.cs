﻿using System;
using System.Windows.Input;

namespace MmvvmHelpers.Commands
{
    /// <summary>
    /// Async -> <see href="https://johnthiriet.com/mvvm-going-async-with-async-command/"/>
    /// </summary>
    public abstract class CommandBase : ICommand
    {
        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        void ICommand.Execute(object parameter) => Execute(parameter);

        bool ICommand.CanExecute(object parameter) => CanExecute(parameter);

        /// <summary>
        /// Wywołanie <see cref="ICommand.Execute(object)"/>
        /// </summary>
        /// <param name="parameter">Parametr polecenia</param>
        protected abstract void Execute(object parameter);

        /// <summary>
        /// Wywołanie <see cref="ICommand.CanExecute(object)"/>
        /// </summary>
        /// <param name="parameter">Parametr polecenia</param>
        /// <returns><see langword="true"/> Jeśli polecenie można wykonać, w przeciwnym razie <see langword="false" /></returns>
        protected abstract bool CanExecute(object parameter);
    }
}
