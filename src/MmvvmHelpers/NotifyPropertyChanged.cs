﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq.Expressions;
using System.Reflection;
using System.Runtime.CompilerServices;

namespace MmvvmHelpers
{
    /// <summary>
    /// Uproszczenie modeli <see cref="INotifyPropertyChanged"/>
    /// <see href="https://rehansaeed.com/model-view-viewmodel-mvvm-part1-overview/"/>
    /// <see href="http://blog.amusedia.com/2013/06/inotifypropertychanged-implementation.html"/> => <see cref="nameof"/>
    /// Uwaga ViewMoidelu nie implemetujemy pól haseł!!! => (Nigdy nie przechowuj w pamięci zwykłych haseł tekstowych).
    /// <see href="https://stackoverflow.com/questions/1483892/how-to-bind-to-a-passwordbox-in-mvvm"/>
    /// 
    /// <see href="https://www.nuget.org/packages/System.Reactive/"/>
    /// <see href="https://rehansaeed.com/reactive-extensions-part1-replacing-events/"/>
    /// </summary>
    public abstract class NotifyPropertyChanged : INotifyPropertyChanged
    {
        /// <summary>
        /// Event informujący o zmianie stanu właściwości.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;
        /// <summary>
        /// Sprawdza czy właściwość została zmieniona, ustawia właściwość, powiadania o zmianie stanu właściwości
        /// </summary>
        /// <typeparam name="T">Typ właściwości</typeparam>
        /// <param name="storage">Odwołanie się do właściwość {get; set} </param>
        /// <param name="value">Żadana wartość właściwość {set = value;} </param>
        /// <param name="propertyName">
        /// Pole może zsotać dostarczone automatycznie patrz <see cref="CallerMemberNameAttribute" />
        /// </param>
        /// <returns>
        /// <c>true</c> jeżeli wartość została zmieniona, <c>false</c> jeżeli stan właściwości = ostatni stan
        /// </returns>
        protected bool SetPropertyChanged<T>(ref T storage, T value, [CallerMemberName] string propertyName = null)
        {
            if (EqualityComparer<T>.Default.Equals(storage, value)) return false;

            storage = value;
            OnPropertyChanged(propertyName);
            return true;
        }

        /// <summary>
        /// Powiadomienie o zmianie stanu właściwości. 
        /// </summary>
        /// <param name="propertyName">
        /// Nazwa właściwości. 
        /// Pole może zsotać dostarczone automatycznie patrz <see cref="CallerMemberNameAttribute" />
        /// </param>
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            VerifyPropertyName(propertyName);
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #region Expression
        private readonly Dictionary<string, object> _values = new Dictionary<string, object>();

        protected void Set<T>(Expression<Func<T>> propertyExpression, T value)
        {
            string propertyName = GetPropertyName(propertyExpression);

            if (string.IsNullOrEmpty(propertyName))
            {
                throw new ArgumentException("Invalid property name", propertyName);
            }

            _values[propertyName] = value;
            OnPropertyChanged(propertyName);
        }

        protected T Get<T>(Expression<Func<T>> propertySelector)
        {
            string propertyName = GetPropertyName(propertySelector);

            if (string.IsNullOrEmpty(propertyName))
            {
                throw new ArgumentException("Invalid property name", propertyName);
            }

            if (!_values.TryGetValue(propertyName, out object value))
            {
                value = default(T);
                _values.Add(propertyName, value);
            }

            return (T)value;
        }

        protected static string GetPropertyName<T>(Expression<Func<T>> propertyExpression)
        {
            if (propertyExpression == null)
            {
                throw new ArgumentNullException("propertyExpression");
            }

            if (!(propertyExpression.Body is MemberExpression body))
            {
                throw new ArgumentException("Invalid argument", "propertyExpression");
            }

            var property = body.Member as PropertyInfo;

            if (property == null)
            {
                throw new ArgumentException("Argument is not a property", "propertyExpression");
            }

            return property.Name;
        }

        #region Debugging
        [Conditional("DEBUG")]
        [DebuggerStepThrough]
        public void VerifyPropertyName(string propertyName)
        {
            if (TypeDescriptor.GetProperties(this)[propertyName] == null)
            {
                if (ThrowOnInvalidPropertyName)
                    throw new ArgumentException("Property not found", propertyName);
                else
                    Debug.Fail("Property not found", propertyName);
            }
        }

        protected virtual bool ThrowOnInvalidPropertyName { get; private set; }
        #endregion

        #endregion
    }
}
