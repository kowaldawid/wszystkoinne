﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Windows;

namespace MmvvmHelpers.Navigation
{
    public class Navigation : INavigation
    {
        public static void Bind(object view, object viewModel)
        {
            if (view is FrameworkElement element && viewModel != null)
            {
                element.DataContext = null;
                element.DataContext = viewModel;
                Debug.WriteLine("--- Bind {0}.DataContext = {1} ---", view.GetType().Name, viewModel.GetType().Name);
            }
        }

        public Navigation(IDictionary<Type, Func<object>> views, IDictionary<Type, Func<object>> viewModels)
        {
            _views = views;
            _viewModels = viewModels;
            contentRegions = new Dictionary<string, IDictionary<Type, Func<object, object>>>();
            pairsVM = new Dictionary<Type, Type>();

            regionViewModel = new Dictionary<string, Type>();
        }

        public void RegisterRegion(string regionName, KeyValuePair<Type, Func<object, object>> getContent) => ContentRegionsAdd(regionName, getContent);

        public void RegisterForNavigation<TView, TViewModel>() => pairsVM.Add(new KeyValuePair<Type, Type>(typeof(TView), typeof(TViewModel)));


        public void RequestNavigate(string regionName, Uri source) => RequestNavigate(regionName, source, null);

        public void RequestNavigate(string regionName, Uri source, IEnumerable<KeyValuePair<string, object>> parameters)
        {

            IEnumerable<KeyValuePair<Type, Func<object, object>>> regions;
            object instanceView;
            object instanceViewModel;

            try
            {
                regions = contentRegions[regionName];


                KeyValuePair<Type, Func<object>> view = _views.FirstOrDefault(v => v.Key.Name == source.ToString());
                instanceView = view.Value?.Invoke();
                instanceViewModel = _viewModels
                    .FirstOrDefault(vm => vm.Key.Equals(pairsVM.FirstOrDefault(x => x.Key == view.Key).Value))
                    .Value?.Invoke();
            }
            catch (KeyNotFoundException)
            {
                Debug.WriteLine(string.Format("==== Region Error {0} it isn't registered ====", regionName));
                throw new ArgumentException($"Wrong {nameof(regionName)} - {regionName}");
            }

            Debug.WriteLine(string.Format("--- Resolve {0} ---", instanceView != null ? instanceView.GetType().FullName : $"Error {source.ToString()}"));
            Debug.WriteLine(string.Format("--- Resolve {0} ---", instanceViewModel != null ? instanceViewModel.GetType().FullName : $"Error ViewModel null -> {source.ToString()}"));


            Bind(instanceView, instanceViewModel);

            if (regionViewModel.TryGetValue(regionName, out Type prevViewModel))
            {
                regionViewModel[regionName] = instanceViewModel.GetType();
            }
            else
            {
                regionViewModel.Add(regionName, instanceViewModel.GetType());
            }

            if (instanceViewModel is INavigationParameters navPar)
                navPar.OnNavigatedTo(parameters ?? new List<KeyValuePair<string, object>>());
#if DEBUG
            else if (parameters != null)
                Debug.WriteLine("==== Wrong: Must implement interface INavigationParameters in the target object ====");
#endif

            foreach (var r in regions)
            {
                r.Value?.Invoke(instanceView);
                Debug.WriteLine("--- Bind Region x:Name=\"{0}\" -> {1} ---", regionName, instanceView != null ? source.ToString() : "null");
            }
        }

        private void ContentRegionsAdd(string key, KeyValuePair<Type, Func<object, object>> valueFactory)
        {
            if (!contentRegions.TryGetValue(key, out IDictionary<Type, Func<object, object>> v))
                contentRegions.Add(key, v = new Dictionary<Type, Func<object, object>>());
            try
            {
                _views[valueFactory.Key] = () => valueFactory.Value.Invoke(null);
            }
            catch (KeyNotFoundException)
            {
                Debug.WriteLine(string.Format("==== View Error {0} it isn't registered ====", valueFactory.Key));
                throw new ArgumentException($"Wrong {nameof(valueFactory.Key)} - {valueFactory.Key}");
            }

            v.Add(valueFactory);
        }

        private readonly IDictionary<Type, Func<object>> _views;
        private readonly IEnumerable<KeyValuePair<Type, Func<object>>> _viewModels;
        private readonly IDictionary<string, IDictionary<Type, Func<object, object>>> contentRegions;
        private readonly IDictionary<Type, Type> pairsVM;

        private readonly Dictionary<string, Type> regionViewModel;    
    }
}
