﻿using System;
using System.Collections.Generic;

namespace MmvvmHelpers.Navigation
{
    public interface INavigation
    {
        void RegisterRegion(string regionName, KeyValuePair<Type, Func<object, object>> getContent);

        void RequestNavigate(string regionName, Uri source);

        void RequestNavigate(string regionName, Uri source, IEnumerable<KeyValuePair<string, object>> parameters);

        void RegisterForNavigation<TView, TViewModel>();
    }
}
