﻿using System.Collections.Generic;

namespace MmvvmHelpers.Navigation
{
    public interface INavigationParameters
    {
        void OnNavigatedTo(IEnumerable<KeyValuePair<string, object>> parameters);
    }
}
