﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Generic
{
    public class Test1
    {

        private static readonly List<ITest> parts = new List<ITest>()
        {
            new TestA<A> {
                TestAction = i => Console.WriteLine(i + typeof(TestA<A>).ToString())
            },
            new TestA<B>  {
                TestAction = i => Console.WriteLine(i + typeof(TestA<B>).ToString())
            },
        };

        //Wersja 1
        public static TestA<T> Get<T>() where T : class => parts.FirstOrDefault(x => x.Type == typeof(T)) as TestA<T>;


        public static void GetDynamic(Type t)
        {
            //Wersja 2
            var temp = parts.FirstOrDefault(x => x.Type == t);
            Type type = typeof(TestA<>).MakeGenericType(t);
            var a1 = Convert.ChangeType(temp, type);

            //Wersja 3
            var a2 = typeof(Test1).GetMethod(nameof(Test1.Get)).MakeGenericMethod(t).Invoke(null, null);
        }

        //Wersja 4
        public static TestA<T> GetDynamic<T>(T t) where T : class => Get<T>();

        public interface ITest
        {
            Type Type { get; }
        }

        public class TestA<T> : ITest, IEquatable<TestA<T>> where T : class
        {
            public Type Type => typeof(T);

            public Action<object> TestAction;

            public bool Equals(TestA<T> other)
            {
                throw new NotImplementedException();
            }
        }
    }
}
