﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Generic
{
    public class Test2
    {
        private static readonly IList<TestB> parts = new List<TestB>()
        {
            new TestB<A> {
                TestAction = i => Console.WriteLine(i + typeof(TestB<A>).ToString())
            },
            new TestB<B>  {
                TestAction = i => Console.WriteLine(i + typeof(TestB<B>).ToString())
            },
        };

        public static TestB Get(Type t) => parts.FirstOrDefault(x => x.Type == t);

        public abstract class TestB
        {
            public virtual Type Type { get; }
            public Action<object> TestAction;
        }

        public class TestB<T> : TestB where T : class, new()
        {
            public override Type Type { get; } = typeof(T);
        }


    }
}
