﻿namespace Generic
{
    class Program
    {
        static void Main(string[] args)
        {
            //Wersja 1
            var a = GetTestCos<A>();
            var b = GetTestCos<B>();

            //Wersja 2 i 3
            Test1.GetDynamic(typeof(A));

            //Wersja 4
            (Test1.GetDynamic(new A { })).TestAction("Test => ");

            //Wersja 5
            Test2.Get(typeof(A)).TestAction("Test => ");
            Test2.Get(typeof(B)).TestAction("Test => ");
        }

        private static bool GetTestCos<T>() where T : class, new()
        {
            Test1.Get<T>().TestAction("Test => ");
            return false;
        }
    }

    public class A
    {
    }

    public class B
    {
    }
}
