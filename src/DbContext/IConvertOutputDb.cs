﻿using System.Collections.Generic;
using System.Data;

namespace DbContext
{
    public interface IConvertOutputDb
    {
        T Convert<T>(object parameter);
        T ConvertToModel<T>(DataTable dataTabl) where T : class;
        IEnumerable<T> ConvertToCollectionModel<T>(DataTable dataTabl) where T : class;
    }
}
