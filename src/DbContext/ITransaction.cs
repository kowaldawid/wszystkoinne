﻿namespace DbContext
{
    public interface ITransaction
    {
        void CommitTransaction();
        void RollbackTransaction();
    }
}
