﻿namespace DbContext
{
    public interface IContext
    {
        IProcedure Procedure { get; }
        IProcedureNonQuery ProcedureNonQuery { get; }
    }
}
