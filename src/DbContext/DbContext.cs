﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace DbContext
{
    public abstract class DbContext : IContext, IConvertOutputDb
    {
        IProcedure IContext.Procedure => _procedure;
        IProcedureNonQuery IContext.ProcedureNonQuery => _procedure;

        private readonly DbProcedure _procedure;

        protected DbContext() => _procedure = new DbProcedure(this);

        protected virtual DataTable Procedure<T>(Action<SqlCommand> operation)
        {
            using (SqlConnection connection = Connection())
            {
                SqlCommand cmd = connection.CreateCommand();
                operation.Invoke(cmd);
                cmd.Connection = connection;
                cmd.CommandType = CommandType.StoredProcedure;
                DataTable dt = new DataTable();
                new SqlDataAdapter(cmd).Fill(dt);
                return dt;
            }
        }

        protected virtual void ProcedureNonQuery(IEnumerable<ProcedureAction> procedureActions)
        {
            using (SqlConnection connection = Connection())
            {
                connection.Open();
                foreach (var operation in procedureActions)
                {
                    SqlCommand cmd = connection.CreateCommand();
                    operation.Operation.Invoke(cmd);
                    cmd.Connection = connection;
                    cmd.CommandType = CommandType.StoredProcedure;
                    if (operation.OutputParameter == null)
                    {
                        cmd.ExecuteNonQuery();
                    }
                    else
                    {
                        cmd.Parameters.Add(operation.OutputParameter);
                        operation.OutputParameter.Direction = ParameterDirection.Output;
                        operation.OutputParameter.Value = DBNull.Value;
                        cmd.ExecuteNonQuery();
                        operation.Callback?.Invoke(operation.OutputParameter.Value);
                    }

                }
            }
        }

        protected abstract SqlConnection Connection();

        public virtual T Convert<T>(object parameter)
        {
            if (parameter == null) return default(T);
            else if (parameter is IComparable) return (T)System.Convert.ChangeType(parameter, typeof(T));
            else return (T)parameter;
        }

        public virtual T ConvertToModel<T>(DataTable dataTabl) where T : class
        {
            throw new NotImplementedException();
        }

        public virtual IEnumerable<T> ConvertToCollectionModel<T>(DataTable dataTabl) where T : class
        {
            throw new NotImplementedException();
        }

        private sealed class DbProcedure : IProcedure, IProcedureNonQuery
        {
            public readonly DbContext context;
            private readonly IList<ProcedureAction> procedures;

            public DbProcedure(DbContext context)
            {
                this.context = context;
                procedures = new List<ProcedureAction>();
            }

            T IProcedure.ExecuteSingleRecordQuery<T>(Action<SqlCommand> operation)
            {
                return context.ConvertToModel<T>(context.Procedure<T>(operation));
            }

            IEnumerable<T> IProcedure.Execute<T>(Action<SqlCommand> operation)
            {
                return context.ConvertToCollectionModel<T>(context.Procedure<T>(operation));
            }

            IProcedureNonQuery IProcedureNonQuery.Add(Action<SqlCommand> operation)
            {
                procedures.Add(new ProcedureAction(this, context, operation));
                return this;
            }

            IProcedureNonQueryCallback IProcedureNonQuery.Add(Action<SqlCommand> operation, SqlParameter outputParameter)
            {
                var test = new ProcedureAction(this, context, operation, outputParameter);
                procedures.Add(test);
                return test;
            }

            void IProcedureNonQuery.Execute()
            {
                context.ProcedureNonQuery(procedures);
                procedures.Clear();
            }
        }

        protected class ProcedureAction : IProcedureNonQueryCallback
        {
            private readonly IProcedureNonQuery dbProcedure;
            private readonly IConvertOutputDb convertOutputDb;

            public Action<SqlCommand> Operation { get; }
            public SqlParameter OutputParameter { get; }

            public Action<object> Callback { get; private set; }

            public ProcedureAction(IProcedureNonQuery dbProcedure,
                IConvertOutputDb convertOutputDb,
                Action<SqlCommand> operation,
                SqlParameter outputParameter)
            {
                this.dbProcedure = dbProcedure;
                this.convertOutputDb = convertOutputDb;
                Operation = operation;
                OutputParameter = outputParameter;
                Callback = null;
            }

            public ProcedureAction(IProcedureNonQuery dbProcedure,
                IConvertOutputDb convertOutputDb,
                Action<SqlCommand> operation)
                : this(dbProcedure, convertOutputDb, operation, null) { }

            IProcedureNonQuery IProcedureNonQueryCallback.Callback<T>(Action<T> callback)
            {
                Callback = (o) => callback.Invoke(convertOutputDb.Convert<T>(o));
                return dbProcedure;
            }
        }
    }
}
