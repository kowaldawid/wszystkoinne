﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace DbContext
{
    public interface IProcedure
    {
        IEnumerable<T> Execute<T>(Action<SqlCommand> operation) where T : class;

        T ExecuteSingleRecordQuery<T>(Action<SqlCommand> operation) where T : class;
    }

    public interface IProcedureNonQuery
    {
        IProcedureNonQuery Add(Action<SqlCommand> operation);

        IProcedureNonQueryCallback Add(Action<SqlCommand> operation, SqlParameter outputParameter);

        void Execute();
    }

    public interface IProcedureNonQueryCallback
    {
        IProcedureNonQuery Callback<T>(Action<T> callback);
    }
}
