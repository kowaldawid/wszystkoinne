﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Transactions;

namespace DbContext
{
    public class DbTransaction : ITransaction
    {
        private readonly TransactionScope scope;

        public DbTransaction() => scope = new TransactionScope();

        public void CommitTransaction() => Commit();

        public void RollbackTransaction() => Destroy();

        private void Commit()
        {
            scope.Complete();
            Destroy();
        }

        private void Destroy() => scope?.Dispose();
    }
}
